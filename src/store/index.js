import Vue from 'vue';
import Vuex from 'vuex';

function createId() {
  let id = Date.now();
  id = String(id).match(/.{1,2}/g);
  for (let i = 0; i < id.length; i += 1) {
    if ((id[i] > 97) && (id[i] < 123)) id[i] = String.fromCharCode(id[i]);
    if ((id[i] > 64) && (id[i] < 91)) id[i] = String.fromCharCode(id[i]);
    if ((id[i] > 47) && (id[i] < 58)) id[i] = String.fromCharCode(id[i]);
  }
  id = id.join('');
  return id;
}

function arrayMove(arr, oldIndex, newIndex) {
  if (newIndex >= arr.length) {
    let k = newIndex - arr.length + 1;
    // eslint-disable-next-line no-plusplus
    while (k--) {
      arr.push(undefined);
    }
  }
  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
  return arr; // for testing
}

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    newNoteOpptions: { id: createId() },
    notes: [],
    sortOptions: [
      { id: 0, property: 'id', direction: 'up' },
      { id: 1, property: 'id', direction: 'down' },
      { id: 2, property: 'number', direction: 'up' },
      { id: 3, property: 'number', direction: 'down' },
      { id: 4, property: 'createTime', direction: 'up' },
      { id: 5, property: 'createTime', direction: 'down' },
      { id: 6, property: 'type', direction: 'up' },
      { id: 7, property: 'type', direction: 'down' },
    ],
    dragStatus: false,
    popupStatus: false,
  },
  mutations: {
    ADD_CARD: (state) => {
      const actualTime = new Date();
      state.newNoteOpptions.createTime = actualTime.getTime();
      state.newNoteOpptions.date = `${actualTime.getDate()}.${actualTime.getMonth() + 1}.${actualTime.getFullYear()} ${actualTime.getHours()}:${actualTime.getMinutes()}:${actualTime.getSeconds()}`;
      state.newNoteOpptions.drag = state.dragStatus;
      state.newNoteOpptions.redactMode = false;
      state.notes.push({ ...state.newNoteOpptions }); // {...} Разрывает связь
      state.newNoteOpptions.id = createId();
    },
    REMOVE_CARD: (state, payload) => {
      state.notes = state.notes.filter((item) => item.id !== payload);
    },
    SORT_CARD: (state, payload) => {
      const sortParameter = state.sortOptions.filter((parameters) => parameters.id === payload)[0];
      const sortDirection = sortParameter.direction;
      const sortProperty = sortParameter.property;

      for (let i = 0; i < state.notes.length; i += 1) {
        for (let k = (i + 1); k < (state.notes.length); k += 1) {
          const findProperty = (sortProperty === 'number' || (sortProperty === 'createTime'))
            ? +state.notes[i][sortProperty] || 0 : state.notes[i][sortProperty];
          const findPropertyNext = ((sortProperty === 'number') || (sortProperty === 'createTime'))
            ? +state.notes[k][sortProperty] || 0 : state.notes[k][sortProperty];

          if ((findPropertyNext < findProperty) && (sortDirection === 'up')) {
            [state.notes[i], state.notes[k]] = [state.notes[k], state.notes[i]];
          }
          if ((findPropertyNext > findProperty) && (sortDirection === 'down')) {
            [state.notes[i], state.notes[k]] = [state.notes[k], state.notes[i]];
          }
        }
      }
      state.notes.splice(state.notes.length, 1); // splice заставляет Vue обновить компоненты
    },
    TOGGLE_DRAG: (state, payload) => {
      state.dragStatus = payload;
      for (let i = 0; i < state.notes.length; i += 1) {
        state.notes[i].drag = state.dragStatus;
      }
    },
    DRAG_CARD: (state, [oldId, beforeId]) => {
      let oldIndex;
      let newIndex;
      for (let i = 0; i < state.notes.length; i += 1) {
        if (state.notes[i].id === oldId) oldIndex = i;
        if (state.notes[i].id === beforeId) newIndex = i;
      }
      arrayMove(state.notes, oldIndex, newIndex);
    },
    REDACT_MODE: (state, id) => {
      for (let i = 0; i < state.notes.length; i += 1) {
        if (state.notes[i].id === id) state.notes[i].redactMode = !state.notes[i].redactMode;
      }
    },
    TOGLE_POPUP: (state) => {
      state.popupStatus = !state.popupStatus;
    },
  },
  actions: {
  },
  modules: {
  },
});
