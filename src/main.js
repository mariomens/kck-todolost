import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import messageFunctionsMixin from '../stubs/mixins/messageFunctionsMixin';

Vue.config.productionTip = false;

Vue.mixin(messageFunctionsMixin);

const app = new Vue({
  router,
  store,
  data: {
    messages: {
      'rowsCounter.of': 'из',
    },
  },
  render: (h) => h(App),
}).$mount('#app');

window.addEventListener('beforeunload', () => {
  const notesString = JSON.stringify(app.$store.state.notes);
  localStorage.setItem('notes', notesString);
});

document.addEventListener('DOMContentLoaded', () => {
  const notesString = localStorage.getItem('notes');
  const notesJSON = JSON.parse(notesString);
  app.$store.state.notes = notesJSON === null ? [] : notesJSON;
});
